package com.greatlearning.employee_management.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.employee_management.entity.Employee;
import com.greatlearning.employee_management.service.EmployeeServiceImpl;

@RestController
@RequestMapping("/api/employees")
public class EmployeeController {

	@Autowired
	EmployeeServiceImpl employeeServiceImpl;

	@GetMapping("test")
	public String TestEndpoint() {
		return "Hello test endpoint";
	}

	@GetMapping
	public List<Employee> fetchAllEmployees() {
		return employeeServiceImpl.findAll();
	}

	@PostMapping
	public Employee addEmployee(@RequestBody Employee employee) {
		employeeServiceImpl.save(employee);
		return employee;
	}
	
	@PutMapping
	public Employee updateEmployee(@RequestBody Employee employee) {
		Employee emp = new Employee();
		emp = employeeServiceImpl.findById(employee.getId()).get();
		emp.setId(employee.getId());
		emp.setFirstName(employee.getFirstName());
		emp.setLastName(employee.getLastName());
		emp.setEmail(employee.getEmail());
		employeeServiceImpl.save(emp);
		return emp;
	}
	

	@DeleteMapping("/{id}")
	public String deleteEmployee(@PathVariable("id") int id) {
		employeeServiceImpl.deleteById(id);
		return "Deleted employee id - " + id;
	}

	@GetMapping("/{id}")
	public Optional<Employee> fetchEmployeeById(@PathVariable("id") int id) {
		return employeeServiceImpl.findById(id);
	}
	
	@GetMapping("/search/{firstName}")
	public List<Employee> searchEmployee(@PathVariable("firstName") String firstName) {
		return employeeServiceImpl.searchEmployeeByFirstName(firstName);
	}
	
	@GetMapping("/sort")
	public List<Employee> SortEmployeeByFirstName(@RequestParam("order") String sortOrder) {
		return employeeServiceImpl.sortEmployeeByFirstNameWithGivenOrder(sortOrder);
	}
}
