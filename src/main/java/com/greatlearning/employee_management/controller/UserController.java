package com.greatlearning.employee_management.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.employee_management.entity.Role;
import com.greatlearning.employee_management.entity.User;
import com.greatlearning.employee_management.service.RoleServiceImpl;
import com.greatlearning.employee_management.service.UserDetailServiceImpl;

@RestController
@RequestMapping("/api")
public class UserController {

	@Autowired
	RoleServiceImpl roleServiceImpl;

	@Autowired
	UserDetailServiceImpl userDetailServiceImpl;

	@PostMapping("/role")
	public Role addRole(@RequestBody Role role) {
		roleServiceImpl.save(role);
		return role;
	}

	@PostMapping("/user")
	public String addUser(@RequestBody User user) {
		userDetailServiceImpl.save(user);
		return "User added";
	}

}
