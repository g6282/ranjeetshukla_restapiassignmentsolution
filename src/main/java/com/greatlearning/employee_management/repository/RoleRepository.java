package com.greatlearning.employee_management.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.greatlearning.employee_management.entity.Role;

public interface RoleRepository extends JpaRepository<Role, Integer> {

}
