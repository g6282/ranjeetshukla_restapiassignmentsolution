package com.greatlearning.employee_management.service;

import java.util.List;
import java.util.Optional;

import com.greatlearning.employee_management.entity.Employee;

public interface EmployeeService {

	public List<Employee> findAll();

	public Optional<Employee> findById(int id);

	public void save(Employee employee);

	public void deleteById(int id);

	public List<Employee> searchEmployeeByFirstName(String firstName);

	public List<Employee> sortEmployeeByFirstNameWithGivenOrder(String Order);
}
