package com.greatlearning.employee_management.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.greatlearning.employee_management.entity.Employee;
import com.greatlearning.employee_management.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeRepository employeeRepository;

	@Override
	public List<Employee> findAll() {
		return employeeRepository.findAll();
	}

	@Override
	public Optional<Employee> findById(int id) {
		return employeeRepository.findById(id);
	}

	@Override
	public void save(Employee employee) {
		employeeRepository.save(employee);
	}

	@Override
	public void deleteById(int id) {
		employeeRepository.deleteById(id);
	}

	@Override
	public List<Employee> searchEmployeeByFirstName(String firstName) {
		return employeeRepository.findEmployeeByFirstName(firstName);
	}

	@Override
	public List<Employee> sortEmployeeByFirstNameWithGivenOrder(String sortOrder) {
		Direction myOrderDirection = Sort.Direction.ASC;
		if (sortOrder.equalsIgnoreCase("desc")) {
			myOrderDirection = Sort.Direction.DESC;
		}
		return employeeRepository.findAll(Sort.by(myOrderDirection, "firstName"));
	}

}
