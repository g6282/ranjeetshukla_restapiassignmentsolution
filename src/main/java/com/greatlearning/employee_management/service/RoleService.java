package com.greatlearning.employee_management.service;

import com.greatlearning.employee_management.entity.Role;

public interface RoleService {
	public void save(Role role);
}
