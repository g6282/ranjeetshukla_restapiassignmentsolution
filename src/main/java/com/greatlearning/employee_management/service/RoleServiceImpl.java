package com.greatlearning.employee_management.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.employee_management.entity.Role;
import com.greatlearning.employee_management.repository.RoleRepository;

@Service
public class RoleServiceImpl implements RoleService {

	@Autowired
	RoleRepository roleRepository;

	@Override
	public void save(Role role) {
		roleRepository.save(role);

	}

}
